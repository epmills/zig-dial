const std = @import("std");
const builtin = @import("builtin");
const process = std.process;
const opts = @import("build_options");

const separator = if (builtin.os.tag == .windows) '\r' else '\n';

const char_translator = fn (c: u8) u8;

pub fn main() !void {

    // Setup memory allocation.
    var page_allocator = std.heap.page_allocator;
    var arena = std.heap.ArenaAllocator.init(page_allocator);
    defer arena.deinit();
    var a = &arena.allocator;

    // Get handles to input, output and error streams.
    const stdin = std.io.getStdIn();
    const in = stdin.inStream();
    const out = std.io.getStdOut().outStream();
    const err = std.io.getStdErr().outStream();

    // Ensure proper arguments.
    try check_args(a, err);

    // Prompt user unless receiving input via pipe.
    if (std.os.isatty(stdin.handle)) {
        try out.print("{}", .{"Enter string to dialize: "});
    }

    // Read a line of text, translating each character to its
    // associated digit on a phone.
    try translate_input(in, out, to_dial);
}

fn translate_input(in: var, out: var, translate: char_translator) !void {
    var line_buf: [1024]u8 = undefined;
    if (try in.readUntilDelimiterOrEof(&line_buf, separator)) |line| {
        for (line) |c| {
            try out.print("{c}", .{translate(c)});
        }
        try out.print("{c}", .{'\n'});
    }
}

fn to_dial(c: u8) u8 {
    return switch (c) {
        '0'...'9' => c,
        'a'...'c', 'A'...'C' => '2',
        'd'...'f', 'D'...'F' => '3',
        'g'...'i', 'G'...'I' => '4',
        'j'...'l', 'J'...'L' => '5',
        'm'...'o', 'M'...'O' => '6',
        'p'...'s', 'P'...'S' => '7',
        't'...'v', 'T'...'V' => '8',
        'w'...'z', 'W'...'Z' => '9',
        else => '*',
    };
}

fn check_args(a: *std.mem.Allocator, out: var) !void {
    const EXIT_USAGE = 64;

    var args = process.args();
    _ = args.skip();
    const arg1 = try args.next(a) orelse "";
    if (std.mem.eql(u8, arg1, "-V") or std.mem.eql(u8, arg1, "--version")) {
        try out.print("{}\n", .{opts.build_id});
        process.exit(EXIT_USAGE);
    } else if (arg1.len > 0) {
        try out.print("usage: {} [-h | --help] [-V | --version]\n", .{opts.app_id});
        process.exit(EXIT_USAGE);
    }
}
