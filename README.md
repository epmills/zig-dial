# zig-dial

Translate a string into its equivalent digits on a phone.

Useful for:
- Translating "vanity" phone numbers to digits only
- Translating web site user names and passwords into phone credentials


## Usage

To enter a string to translate, simply execute *dialz*, enter the value
and press `Return`:
```shell
$ dialz
Enter string to dialize: hello world!
43556*96753*
```

You can also pipe a string from a file or another command into *dialz*:
```shell
$ echo $mysecret | dialz
43556*96753*
```

To show usage and options:
```shell
$ dialz --help
usage: dialz [-h | --help] [-V | --version]
```

To show the version (build ID):
```shell
$ dialz --version
dialz 0.1.2 (499296de6 2020-07-19)
```


## Installation

The easiest way to install `dialz` is via [Homebrew][]:

```shell
$ brew tap epmills/tap https://gitlab.com/epmills/homebrew-tap.git
```
This only needs to be done once.

Then, to install `dialz`:
```shell
$ brew install dialz
```

NOTE: The Homebrew installation has been used successfully on MacOS.
These instructions should work for Linux and Windows Subsystem for
Linux (WSL) but have not been validated.

If you prefer to install manually, follow the instructions below for
your OS.

### Linux

Download `dialz_x86_64-linux-musl.tar.gz` from [releases][]

Navigate to the folder where the archive was downloaded
```shell
$ cd ~/Downloads
```

Extract the files in the archive:
```shell
$ tar xvzf dialz_x86_64-linux-musl.tar.gz
```

Finally, copy it somewhere in your path:
```shell
$ cp dialz/dialz /usr/local/bin
```

### MacOS

Download `dialz_x86_64-macosx-gnu.tar.gz` from [releases][]

Navigate to the folder where the archive was downloaded
```shell
$ cd ~/Downloads
```

Extract the files in the archive:
```shell
$ tar xvzf dialz_x86_64-macosx-gnu.tar.gz
```

If you're using MacOS 10.15 (Catalina) or above, you must take the binary out
of quarantine to execute it:
```shell
$ xattr -r -d com.apple.quarantine dialz/dialz
```

Finally, copy it somewhere in your path:
```shell
$ cp dialz/dialz /usr/local/bin
```


### Windows

Download `dialz_x86_64-windows-gnu.zip` from [releases][]

Navigate to the folder where the binary was downloaded
```shell
C:\> CD %HOME%\Downloads
```

Extract the files in the archive:
```shell
$ unzip dialz_x86_64-windows-gnu.zip
```


Finally, copy it somewhere in your path:
```shell
C:\Users\Me\Downloads> COPY dialz\dialz.exe %ProgramFiles%
```


## Caveats

It would have been preferable not to echo the input because one of the main
use cases for this binary is to translate passwords into digits.  However,
prompting the user for input without echoing it is very platform-specific.

It was more important to produce a Windows binary that to provide this
no-echo functionality.  But hopefully in the future, this feature can be
added.

As a result, it may be advisable to clear the screen after executing *dialz* and copying the output.


[Homebrew]: https://brew.sh/
[releases]: https://gitlab.com/epmills/zig-dial/-/releases
