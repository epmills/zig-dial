# Makefile for epmills/zig-dial
#  - Must be run from MacOS host

.PHONY: clean

APP_ID = dialz
SRC_FILES = main.zig build.zig

BUILD_OPTS = -Drelease-fast=true

TRIPLE_NATIVE = native
TRIPLE_MACOS = x86_64-macosx-gnu
TRIPLE_LINUX = x86_64-linux-musl
TRIPLE_WINDOWS = x86_64-windows-gnu

MAKEFILE_DIR = $(shell pwd)
DIST_DIR = $(MAKEFILE_DIR)/dist
CACHE_DIR = $(MAKEFILE_DIR)/zig-cache
CACHE_BIN_DIR = $(CACHE_DIR)/bin

clean:
	rm -fR $(CACHE_DIR) $(DIST_DIR)
dist: archives

# Binaries
binaries: macos.binary linux.binary windows.binary

macos.binary: $(SRC_FILES)
	zig build $(BUILD_OPTS) -Dtarget=$(TRIPLE_NATIVE)

linux.binary: $(SRC_FILES)
	zig build $(BUILD_OPTS) -Dtarget=$(TRIPLE_LINUX)

windows.binary: $(SRC_FILES)
	zig build $(BUILD_OPTS) -Dtarget=$(TRIPLE_WINDOWS)

# Archives
archives: linux.archive macos.archive windows.archive

dist.dir:
	mkdir -p $(DIST_DIR)

linux.archive: dist.dir linux.binary
	(cd $(DIST_DIR); \
	  mkdir -p tmp.$(TRIPLE_LINUX)/$(APP_ID); \
	  cp ../LICENSE ../README.md tmp.$(TRIPLE_LINUX)/$(APP_ID); \
	  cd tmp.$(TRIPLE_LINUX); \
	  cp $(CACHE_BIN_DIR)/$(APP_ID)*$(TRIPLE_LINUX)* $(APP_ID)/$(APP_ID); \
	  cd $(APP_ID); \
	  sha256sum * > sha256sums.txt; \
	  cd ..; \
	  tar cvzf ../$(APP_ID)_$(TRIPLE_LINUX).tar.gz $(APP_ID)/*; \
	)
	rm -R $(DIST_DIR)/tmp.$(TRIPLE_LINUX);

macos.archive: dist.dir macos.binary
	(cd $(DIST_DIR); \
	  mkdir -p tmp.$(TRIPLE_MACOS)/$(APP_ID); \
	  cp ../LICENSE ../README.md tmp.$(TRIPLE_MACOS)/$(APP_ID); \
	  cd tmp.$(TRIPLE_MACOS); \
	  cp $(CACHE_BIN_DIR)/$(APP_ID)*$(TRIPLE_MACOS)* $(APP_ID)/$(APP_ID); \
	  cd $(APP_ID); \
	  sha256sum * > sha256sums.txt; \
	  cd ..; \
	  tar cvzf ../$(APP_ID)_$(TRIPLE_MACOS).tar.gz $(APP_ID)/*; \
	)
	rm -R $(DIST_DIR)/tmp.$(TRIPLE_MACOS);

windows.archive: dist.dir windows.binary
	(cd $(DIST_DIR); \
	  mkdir -p tmp.$(TRIPLE_WINDOWS)/$(APP_ID); \
	  cp ../LICENSE ../README.md tmp.$(TRIPLE_WINDOWS)/$(APP_ID); \
	  cd tmp.$(TRIPLE_WINDOWS); \
	  cp $(CACHE_BIN_DIR)/$(APP_ID)*$(TRIPLE_WINDOWS)*.exe $(APP_ID)/$(APP_ID).exe; \
	  cd $(APP_ID); \
	  sha256sum * > sha256sums.txt; \
	  cd ..; \
	  zip ../$(APP_ID)_$(TRIPLE_WINDOWS) $(APP_ID)/*; \
	)
	rm -R $(DIST_DIR)/tmp.$(TRIPLE_WINDOWS);
